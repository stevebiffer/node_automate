/** @module {{variableName}}View */
define(['{{filename}}'], function() {
    'use strict';
    /** @class {{variableName}}View */
    var {{variableName}}View = Object.create(Object);
    return {{variableName}}View;
});
