/** @module {{variableName}}Control */
define(['{{pathToFile}}'], function() {
    'use strict';
    
    /** @class {{variableName}}Control */
    var {{variableName}}Control = Object.create(Object);

    

    return {{variableName}}Control;
});
