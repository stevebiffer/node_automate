define(['{{pathToFile}}/_Model', '{{pathToFile}}/_View', '{{pathToFile}}/_Control'], function(Model, View, Control) {
    'use strict';
    var {{filename}} = Object.create();
    return {{filename}};
});
