/** @module {{variableName}}Model */
define(['{{pathToFile}}'], function() {
    'use strict';
    
    /** @class {{variableName}}Model */
    var {{variableName}}Model = Object.create(Object);

    

    return {{variableName}}Model;
});
