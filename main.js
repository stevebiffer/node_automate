#!/usr/bin/env/ node
 // @ts-check
const fs = require('fs');
const path = require('path');
const Handlebars = require('handlebars');
const inquirer = require('inquirer');
const OPTIONS = fs.readdirSync(__dirname + '/templates');
const CURR_DIR = process.cwd();
const PREFIX = 'KK_';
var projectName = null;

/**
 * @param {string} word
 * @return {string} upperCamelCase
 */
function toUpperCamelCase(word) {
  return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
};

/**
 * @param {string} word
 * @return {string} variableName
 */
function toVariableName(word) {
  var variableName;
  var splits = word.split('_');
  splits[0] = splits[0].toLowerCase();
  for (let i = 1; i < splits.length; i++) {
    splits[i] = toUpperCamelCase(splits[i]);
  }
  variableName = splits.join('');
  return variableName;
}

var QUESTIONS = [{
    name: 'project-choice',
    type: 'list',
    message: 'Welche Projektvorlage möchtest du generieren?',
    choices: OPTIONS,
  },
  {
    name: 'project-name',
    type: 'input',
    message: 'Projektname',
    validate: function(input) {
      if (/^([A-Za-z\-\_\d])+$/.test(input)) {
        return true;
      } else {
        return 'Projekte dürfen nur Buchstaben,' +
          ' Zahlen, Unterstriche und Hashes enthalten.';
      }
    },
  },
];
inquirer.prompt(QUESTIONS).then((answers) => {
  const projectChoice = answers['project-choice'];
  const templatePath = __dirname + '/templates/' + projectChoice;
  projectName = answers['project-name'];
  fs.mkdirSync(CURR_DIR + '/' + projectName);
  createDirectoryContents(templatePath, projectName);
});

/**
 *
 *
 * @param {string} templatePath
 * @param {string} newProjectPath
 */
function createDirectoryContents(templatePath, newProjectPath) {
  const filesToCreate = fs.readdirSync(templatePath);


  filesToCreate.forEach( /** @param {string} file */ (file) => {
    const origFilePath = path.join(templatePath, file);
    const stats = fs.statSync(origFilePath);
    console.log(file);
    // get stats about the current file
    if (stats.isFile()) {
      const contents = fs.readFileSync(origFilePath, 'utf8');
      const template = Handlebars.compile(contents);
      const data = {
        'filename': projectName,
        'pathToFile': newProjectPath,
        'variableName': toVariableName(projectName),
      };
      var filename = PREFIX + '_' + projectName + file;
      const compiled = template(data);

      if (file.endsWith('_main.js')) {
        filename = (PREFIX + '_' + projectName).toLowerCase()+'.js';
      } else if (file.endsWith('package.json')) {
        filename = 'package.json';
      } else {
        filename = PREFIX+toUpperCamelCase(projectName + file);
      }

      const writePath = path.join(CURR_DIR, newProjectPath, filename);
      fs.writeFileSync(writePath, compiled, 'utf8');
    } else if (stats.isDirectory()) {
      fs.mkdirSync(path.join(CURR_DIR, newProjectPath, file));
      // recursive call for nested folders / files
      const source = path.join(templatePath, file);
      const target = path.join(newProjectPath, file);
      createDirectoryContents(source, target);
    }
  });
}
